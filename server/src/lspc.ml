(* module J = Lsplib__Hh_json *)
module LSP = Lsplib__Lsp
module LSPf = Lsplib__Lsp_fmt

type command = LSP.lsp_message

let get_next_command () : command Lwt.t =
  let%lwt req = Lsp_io.read_request Lwt_io.stdin in
  LSPf.parse_lsp req (fun _ -> raise Not_found)
  |> Lwt.return

let send_message (m: LSP.lsp_message) : unit Lwt.t =
  m |> LSPf.print_lsp |> Lsp_io.send_json Lwt_io.stdout

let show message : unit Lwt.t =
  let msg = let open LSP.ShowMessage in
    { type_ = LSP.MessageType.InfoMessage ; message } in
  LSP.(NotificationMessage (ShowMessageNotification msg)) |> send_message

let pp_command fmt c =
  (*LSPf.print_lsp c |> J.json_to_string *)
  LSPf.denorm_message_to_string c
  |> Format.fprintf fmt "%s"
