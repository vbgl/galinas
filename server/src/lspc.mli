module LSP = Lsplib__Lsp

type command = LSP.lsp_message

val get_next_command : unit -> command Lwt.t

val send_message : LSP.lsp_message -> unit Lwt.t

val show : string -> unit Lwt.t

val pp_command : Format.formatter -> command -> unit
