type t = {
  uri: string;
  version: int;
  text: string;
  process: Lwt_process.process;
}

let rec handle_input chan log : unit Lwt.t =
  let open Lwt.Infix in
  (try%lwt Lwt_io.read_line chan >>= Lwt.return_some
   with Lwt_io.Channel_closed _ -> Lwt.return None)
  >>= function
  | None -> Lwt.return_unit
  | Some ans ->
    Format.fprintf log "Got: %s.@." ans;
    (*let%lwt () = Lspc.show ans in*)
    handle_input chan log

let send_text text chan : unit Lwt.t =
  let%lwt () = Lwt_io.fprintlf chan "(Add () %S)" text in
  Lwt_io.flush chan

let destroy (doc: t) : unit Lwt.t =
  let%lwt _ = doc.process # close in
  Lwt.return_unit

let create ~uri ~version ~text log : t Lwt.t =
  let process = Lwt_process.open_process
      ("", [| Config.sertop ; "--async-full" ; "--printer=human" |])
  in
  let%lwt () = send_text text process#stdin in
  Lwt.async (fun () -> handle_input process#stdout log);
  Lwt.return { uri ; version ; text ; process }

let set_version x ~version : t =
  { x with version }

let set_text x ~text : t =
  { x with text }
