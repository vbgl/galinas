module LSP = Lsplib__Lsp

let log, clean =
  let oc = open_out "/tmp/galinas.log" in
  Format.formatter_of_out_channel oc, fun () -> close_out oc

let do_initialize ~id =
  let r =
    let open LSP.Initialize in
    {
      server_capabilities = {
        textDocumentSync = {
          want_openClose = true;
          want_change = FullSync;
          want_willSave = false;
          want_willSaveWaitUntil = false;
          want_didSave = None;
        };
        hoverProvider = false;
        completionProvider = None;
        signatureHelpProvider = None;
        definitionProvider = false;
        referencesProvider = false;
        documentHighlightProvider = false;
        documentSymbolProvider = false;
        workspaceSymbolProvider = false;
        codeActionProvider = false;
        codeLensProvider = None;
        documentFormattingProvider = false;
        documentRangeFormattingProvider = false;
        documentOnTypeFormattingProvider = None;
        renameProvider = false;
        documentLinkProvider = None;
        executeCommandProvider = None;
        typeCoverageProvider = false;
        rageProvider = false;
      }
    }
  in LSP.(ResponseMessage (id, InitializeResult r)) |> Lspc.send_message

let documents : (string, CoqDocument.t) Hashtbl.t = Hashtbl.create 17

let do_open ~params : unit Lwt.t =
  let { LSP.DidOpen.textDocument } = params in
  let open LSP.TextDocumentItem in
  let { uri ; version ; text ; languageId = _ } = textDocument in
  let%lwt doc = CoqDocument.create ~uri ~version ~text log in
  if Hashtbl.mem documents uri then (Format.fprintf log "Unclosed document: %s.@." uri; Hashtbl.remove documents uri);
  Hashtbl.add documents uri doc;
  "Open: " ^ uri |> Lspc.show

let do_close ~params : unit Lwt.t =
  let { LSP.DidClose.textDocument } = params in
  let { LSP.TextDocumentIdentifier.uri } = textDocument in
  match Hashtbl.find documents uri with
  | exception Not_found -> Format.fprintf log "Closing unexisting document: %s.@." uri; Lwt.return_unit
  | doc ->
    Hashtbl.remove documents uri;
    CoqDocument.destroy doc

let apply_change doc event : CoqDocument.t =
  let { LSP.DidChange.text ; range = _ ; rangeLength = _ } = event in
  CoqDocument.set_text doc ~text

let do_change ~params : unit =
  let { LSP.DidChange.textDocument; contentChanges } = params in
  let { LSP.VersionedTextDocumentIdentifier.uri ; version } = textDocument in
  let doc = Hashtbl.find documents uri in
  let doc = List.fold_left apply_change doc contentChanges in
  Hashtbl.replace documents uri (CoqDocument.set_version doc ~version)

type outcome = Continue | Done

let continue f = let%lwt () = f in Lwt.return Continue

let ( <| ) f k = k f

let exec_command : Lspc.command -> outcome Lwt.t =
  function
  | LSP.(RequestMessage (_, ShutdownRequest)) -> Lwt.return Done
  | LSP.(RequestMessage (id, InitializeRequest _)) ->
    Format.fprintf log "Got: Initialize.@.";
    do_initialize ~id <| continue
  | LSP.(NotificationMessage InitializedNotification) -> Lwt.return Continue
  | LSP.(NotificationMessage (DidOpenNotification params)) -> do_open ~params <| continue
  | LSP.(NotificationMessage (DidCloseNotification params)) -> do_close ~params <| continue
  | LSP.(NotificationMessage (DidChangeNotification params)) -> do_change ~params; Lwt.return Continue
  | m -> Format.fprintf log "Got: %a.@." Lspc.pp_command m; Lwt.return Continue

let rec loop () =
  let open Lwt.Infix in
  Lspc.get_next_command () >>= exec_command >>= function
  | Continue -> loop ()
  | Done -> Lwt.return_unit

let () =
  Lwt_main.run (loop ());
  clean ();
  Format.printf "Fini.@."
