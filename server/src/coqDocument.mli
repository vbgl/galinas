type t
val create : uri:string -> version:int -> text:string -> Format.formatter -> t Lwt.t
val destroy : t -> unit Lwt.t
val set_version : t -> version:int -> t
val set_text : t -> text:string -> t
