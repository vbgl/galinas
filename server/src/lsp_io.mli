val read_request : Lwt_io.input_channel -> Lsplib__Hh_json.json Lwt.t
val send_json : Lwt_io.output_channel -> Lsplib__Hh_json.json -> unit Lwt.t
