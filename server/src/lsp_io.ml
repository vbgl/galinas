(************************************************************************)
(* The λΠ-modulo Interactive Proof Assistant                            *)
(************************************************************************)

(************************************************************************)
(* λΠ-modulo serialization Toplevel                                     *)
(* Copyright 2018 MINES ParisTech -- Dual License LGPL 2.1 / GPL3+      *)
(* Written by: Emilio J. Gallego Arias                                  *)
(************************************************************************)
(* Status: Very Experimental                                            *)
(************************************************************************)

module F = Format
module J = Lsplib__Hh_json

let debug_fmt = ref F.err_formatter

let _log_error hdr msg =
  F.fprintf !debug_fmt "[%s]: @[%s@]@\n%!" hdr msg

let log_object hdr obj =
  F.fprintf !debug_fmt "[%s]: @[%s@]@\n%!" hdr (J.json_to_string ~pretty:true obj)

exception ReadError of string

let read_request ic =
  let%lwt cl = Lwt_io.read_line ic in
  match String.split_on_char ':' cl with
  | [ "Content-Length" ; size ] ->
    begin try%lwt
      let size = size |> String.trim |> int_of_string in
      let buf = Bytes.create size in
      (* Consume the second \r\n *)
      let%lwt _ = Lwt_io.read_line ic in
      let%lwt () = Lwt_io.read_into_exactly ic buf 0 size in
      Bytes.to_string buf |> J.json_of_string |> Lwt.return
    with Failure msg -> Lwt.fail (ReadError (cl ^ ": " ^ msg))
  end
  | _ -> Lwt.fail (ReadError cl)

let send_json out obj =
  log_object "send" obj;
  let msg  = J.json_to_string ~pretty:false obj in
  let size = String.length msg         in
  let%lwt () = Lwt_io.fprintf out "Content-Length: %d\r\n\r\n%s" size msg in
  Lwt_io.flush out
