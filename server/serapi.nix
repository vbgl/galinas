{ fetchFromGitHub, coq_8_9, ocamlPackages, dune }:

let coq = coq_8_9; in

ocamlPackages.buildDunePackage {
  pname = "coq-serapi";
  version = "8.9";
  src = fetchTarball https://github.com/ejgallego/coq-serapi/tarball/v8.9;

  patches = [ ./serapi.patch ];

  buildInputs = [ coq ] ++ (with ocamlPackages; [ cmdliner ppx_import ppx_sexp_conv sexplib camlp5 ]);
}
