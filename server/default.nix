{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let lsplib =
  let inherit (ocamlPackages) buildDunePackage core; in
  buildDunePackage rec {
    pname = "lsplib";
    version = "0.0.0-20181101";
    src = fetchFromGitHub {
      owner = "vbgl";
      repo = pname;
      rev = "feeb5854a00f946bf34462f7de4b989d520ddce4";
      sha256 = "0lscbihfznbgknb8yfvn7xk1gvilw816faqwwakk8izg2faadl3s";
    };
    propagatedBuildInputs = [ core ];
  }
; in

let coqP = coqPackages_8_9; in

#assert coqP.coq.ocamlPackages == ocamlPackages;

let inherit (ocamlPackages) buildDunePackage ppx_tools_versioned merlin lwt_ppx; in

let serapi = callPackage ./serapi.nix {}; in

buildDunePackage rec {
  version = "0.0.0-dev";
  pname = "galinas";
  src = ./.;

  buildInputs = [ which merlin ppx_tools_versioned lwt_ppx lsplib coqP.coq ocamlPackages.camlp5 serapi ];

  meta = {};
}
