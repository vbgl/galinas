.PHONY: init build

build:
	(cd vsc-galinas && npm install)

init:
	mkdir -p $(HOME)/.vscode/extensions
	$(RM) $(HOME)/.vscode/extensions/vsc-galinas
	ln -s $(PWD)/vsc-galinas/ $(HOME)/.vscode/extensions/
