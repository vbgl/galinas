# galinas

Experimenting user interfaces for the Coq proof assistant

## Getting started

 1. Symlink the `vsc-galinas` directory into `~/.vscode/extensions/`.
 2. Run `npm install` in that repository.
 3. Install the server (`nix-env -if server`).
 4. Run VS-Code and visit a `.v` file.

## Licensing information

This work is partially derived from:

  - [VSCoq](https://github.com/siegebell/vscoq) by Christian J. Bell, distributed under the terms of the MIT license
    (see ./vsc-galinas/License-vscoq.text).
